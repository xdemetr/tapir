const cardHolder = document.getElementById('card-holder');

cardHolder.onkeyup = function () {
  this.value = this.value.toUpperCase();
};

cardHolder.onblur = function () {
  validateCardHolder(this);
};

function validateCardHolder(input) {
  if (input.value.length < 4) {
    input.classList.add('form-field_invalid');
  } else {
    input.classList.remove('form-field_invalid');
  }
}
