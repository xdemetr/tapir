const inputs = document.getElementsByClassName('form-field_for_card-number');
Array.prototype.forEach.call(inputs, function(item){
  item.onkeyup = function() {
    test(this);
    validate(this);
    return false;
  };

  item.oninput = function () {
    validate(this);
    test(this);
    return false;
  };
});

function test(obj) {
  if (obj.value.length == 4) {
    var next = obj.nextSibling;
    while(next.nodeType != 1 && next.nextSibling)
      next = next.nextSibling;
    if (next.nodeType == 1)
      next.focus();
  }
}

function validate(input) {
  input.value = input.value.replace(/[^0-9]/, "");
}
