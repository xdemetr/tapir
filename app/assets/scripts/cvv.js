const cvv = document.getElementById('cvv');

cvv.onkeyup = function () {
  this.value = this.value.toUpperCase();
  validateCVV(this);
};

cardHolder.onblur = function () {
  validateCardHolder(this);
};

function validateCVV(input) {
  if (input.value.length < 3) {
    input.classList.add('form-field_invalid');
  } else {
    input.classList.remove('form-field_invalid');
  }
  
  input.value = input.value.replace(/[^0-9]/, "");
}
