const form = document.getElementById('payment');

form.onsubmit = function () {
  const inputs = form.getElementsByTagName('input');

  Array.prototype.forEach.call(inputs, function(item){
    if (item.value.length == 0) {
      item.classList.add('form-field_invalid');
    } else {
      item.classList.remove('form-field_invalid');
    }
  });
  
  return false;
};
